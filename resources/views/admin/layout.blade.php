<h1>Admin</h1>
<div class="leftnav" style="float: left; width: 150px; height: 100%; margin-right: 30px;">
    <b>Students</b>
    <ul>
        <li><a href="/admin/">list</a></li>
        <li><a href="/admin/create">add</a></li>
    </ul>
    <ul>
        <li><a href="/view">Frontend</a></li>
    </ul>
</div>
<div>
    @yield('content')
</div>
