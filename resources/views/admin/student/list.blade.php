@extends('admin.layout')
@section('content')
    <h2>Students list</h2>
    <table border="1" style="text-align: center;">
        <th>Name</th>
        <th>Unsubmitted tasks</th>
        <th>Rating</th>
        <th>Action</th>
        @foreach ($students as $student)
            <tr>
                <td>
                    <a href="/view/{{ $student->student_id }}">{{ $student->name }}</a>
                </td>
                <td>@foreach($indebtedness as $studentDebt)
                        @foreach($tasks as $task)
                            @if($studentDebt->student_id == $student->student_id
                                && $studentDebt->task_id == $task->task_id)

                                {{ $task->task }}
                                <br />
                            @endif
                        @endforeach
                    @endforeach
                </td>
                <td>{{ $student->rating }}</td>
                <td>
                    <a href="/admin/{{ $student->student_id }}/edit">edit</a>
                    <form style="float:right; padding: 0 15px;"
                          action="/admin/{{ $student->student_id }}"method="POST">
                        {{ method_field('DELETE') }}

                        {{ csrf_field() }}
                        <button>Delete</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
