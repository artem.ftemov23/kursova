@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Edit</h2>
    <form action="/admin/{{ $student->student_id }}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <label>Name </label>
        <input type="text" name="name" value="{{ $student->name }}">
        <br/><br/>
        <label>Rating </label>
        <input type="number" name="rating" value="{{ $student->rating }}">
        <br/><br/>
        <label>Indebtedness </label>

        @foreach($tasks as $task)
            {{ $task->task }}
            <input type="checkbox", name="{{ $task->task }}"
                   @foreach($indebtedness as $studentDebt)
                       @if($task->task_id == $studentDebt->task_id)
                           checked
                       @endif
                   @endforeach
            >

        @endforeach
        <br/><br/>
        <input type="submit" value="Зберегти">
    </form>
@endsection
