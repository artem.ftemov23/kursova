@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Додати групу</h2>
    <form action="/admin" method="POST">
        {{ csrf_field() }}
        <label>Name </label>
        <input type="text" name="name">
        <br/><br/>
        <label>Rating </label>
        <input type="number" name="rating">
        <br/><br/>
        <label>Indebtedness </label>

        @foreach($tasks as $task)
            {{ $task->task }}
            <input type="checkbox", name="{{ $task->task }}">

        @endforeach
        <br/><br/>
        <input type="submit" value="Зберегти">
    </form>
@endsection
