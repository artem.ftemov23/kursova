<html>
    <head>
        <title>Information system "Subject"</title>
    </head>
    <body>
        @yield('page_title')

        <br/><br/>

        <div class="container">
            Main information
            @yield('content')
        </div>
        <br/>
    </body>

    @include('app.layouts.footer')
</html>
