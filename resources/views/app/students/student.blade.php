@extends('app.layouts.layout')
@section('page_title')
    <b>Information about student {{ $profile->name }}</b>
@endsection
@section('content')
    <p>Name: {{ $profile->name }}</p>
    <p>Unsubmitted tasks:
        @foreach($indebtedness as $studentDebt)
            @if($studentDebt->student_id == $profile->student_id)
                {{ $studentDebt->task }}
            @endif
        @endforeach
    </p>
    <p>Rating: {{ $profile->rating }}</p>
    <a href="/view">See information about all students</a>
@endsection
