@extends('app.layouts.layout')
@section('page_title')
    <b>Students list</b>
@endsection

@section('content')
    <form method="get" action="/view">
        <input type="text", name="student", value="{{ $student_selected }}">
        <input type="submit" value="Find" />
    </form>
    <table border="1">
        <th>Id</th>
        <th>Name</th>
        <th>Indebtedness</th>
        @foreach ($students as $student)
            <tr>
                <td>
                    <a href="/view/{{ $student->student_id }}">
                        {{ $student->student_id }}
                    </a>
                </td>
                <td>{{ $student->name }}</td>
                <td>
                    @foreach($indebtedness as $studentDebt)
                        @if($studentDebt->student_id == $student->student_id)
                            {{ $studentDebt->task }}
                            <br />
                        @endif
                    @endforeach
                </td>
            </tr>
        @endforeach
    </table>
    <br/>
    <a href="admin">Admin panel</a>
@endsection
