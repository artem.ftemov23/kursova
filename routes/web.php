<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentsController;
use App\Http\Controllers\AdminStudentController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/view', [StudentsController::class, 'view']);
Route::get('/view/{student}', [StudentsController::class, 'student']);

Route::resource('/admin', AdminStudentController::class);
