<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Students extends Model
{
    use HasFactory;

    public function getStudents($student_name, $task){
        $subjects = DB::table('indebtedness');
        $subjects->select( 'students.*', 'name')->distinct()
            ->join('tasks', 'indebtedness.task_id', '=', 'tasks.task_id')
            ->join('students', 'students.student_id', '=', 'indebtedness.student_id')
            ->orderBy('name');

        if ($student_name)
            $subjects->where('name', 'like', $student_name);
        if ($task)
            $subjects->where('task_id', 'like', $task);

        return $subjects->get();
    }

    public function getIndebtedness(){
        $indebtedness = DB::table('indebtedness');
        $indebtedness->select( 'indebtedness.task_id', 'indebtedness.student_id', 'tasks.task')
            ->join('tasks', 'indebtedness.task_id', '=', 'tasks.task_id')
            ->join('students', 'students.student_id', '=', 'indebtedness.student_id');

        return $indebtedness->get();
    }

    public function getTasks(){
        $tasks = DB::table('tasks');
        $tasks->select('task_id', 'task');
        return $tasks->get();
    }

    public function getProfile($profile_id){
        if(!$profile_id) return null;
        $profile = DB::table('students')
            ->select('*')
            ->where('student_id', $profile_id)
            ->get()->first();
        return $profile;
    }
}
