<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Indebtedness extends Model
{
    use HasFactory;

    protected $table = 'indebtedness';
    protected $primaryKey = 'student_id';
    public $timestamps = false;
}
