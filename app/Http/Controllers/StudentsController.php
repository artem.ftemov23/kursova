<?php

namespace App\Http\Controllers;

use App\Models\Students;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    public function view(Request $request){
        $student = $request->input('student');
        $task = $request->input('task');

        $studentModel = new Students();
        $students = $studentModel->getStudents($student, $task);
        $indebtedness = $studentModel->getIndebtedness();
        $taskList = $studentModel->getTasks();
        return  view('app.students.list', [
                'students' => $students,
                'indebtedness' => $indebtedness,
                'task_selected' => $task,
                'student_selected' => $student,
                'tasks' => $taskList]);
    }

    public function student($id) {
        $profile_model = new Students();
        $profile = $profile_model->getProfile($id);
        $indebtedness = $profile_model->getIndebtedness();
        return view('app.students.student', [
                'profile' => $profile,
                'indebtedness' => $indebtedness
        ]);
    }
}
