<?php

namespace App\Http\Controllers;

use App\Models\Indebtedness;
use App\Models\Student;
use App\Models\Task;
use App\Models\Students;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rules\In;
use App\Models\Flight;

class AdminStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::get();
        $indebtedness = Indebtedness::get();
        $tasks = Task::get();
        return view('admin.student.list', [
            'students' => $students,
            'indebtedness' => $indebtedness,
            'tasks' => $tasks
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tasks = Task::get();
        return view('admin.student.add', ['tasks' => $tasks]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $students = Student::get();
        $tasks = Task::get();
        $name = $request->input('name');
        $rating = $request->input('rating');

        $student = new Student();
        $student->name = $name;
        $student->rating = $rating;

        $idCheker = 0;
        foreach ($students as $studentIdCheck) {
            $idCheker++;
            if($studentIdCheck->student_id > $idCheker){
                $student->student_id = $idCheker;
                break;
            }
        }

        $student->save();

        foreach($tasks as $task){
            $studentsDebt = $request->input($task->task);
            if($studentsDebt){
                $indebtedness = new Indebtedness();
                $indebtedness->task_id = $task->task_id;
                $indebtedness->student_id = $student->student_id;
                $indebtedness->save();
            }
        }


        return Redirect::to('/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tasks = Task::get();
        $student = Student::where('student_id', $id)->first();
        $indebtedness = Indebtedness::where('student_id', $id)->get();
        return view('admin.student.edit', [
            'student' => $student,
            'indebtedness' => $indebtedness,
            'tasks' => $tasks
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tasks = Task::get();
        $student = Student::where('student_id', $id)->first();
        $student->name = $request->input('name');
        $student->rating = $request->input('rating');
        $student->save();

        foreach($tasks as $task) {
            $studentsDebt = $request->input($task->task);
            $existingIndebtedness = Indebtedness::where('student_id', $id)->where('task_id', $task->task_id)->get();
            if($studentsDebt && !$existingIndebtedness->first()) {
                    $indebtedness = new Indebtedness();
                    $indebtedness->task_id = $task->task_id;
                    $indebtedness->student_id = $student->student_id;
                    $indebtedness->save();
            } elseif(!$studentsDebt && $existingIndebtedness) {
                Indebtedness::where('student_id', $id)->where('task_id', $task->task_id)->delete();
            }

        }

        return Redirect::to('/admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::destroy($id);
        Indebtedness::where('student_id', $id)->delete();
        return Redirect::to('/admin');
    }
}
